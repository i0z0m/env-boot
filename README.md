# Acknowledgments
- `download.sh` and `launch.sh` and `ssh_config.sh`  
[tkmru/multiarch_bootstrap](https://github.com/tkmru/multiarch_bootstrap)

- `mount.sh` and `unmount.sh`  
[ntddk/dotfiles](https://github.com/ntddk/dotfiles)
